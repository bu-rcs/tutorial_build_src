# tutorial_build_src

Teaching materials for my "Building Software from Source Code on Linux" tutorial.

This lesson is designed for live tutorials offered at Boston University through the Research Computing Services group. Feel free to use and modify the material as you like. I also encourage you to contribute any corrections and improvements that you may make.

The tutorial description is as follows:

> Compiling a working executable from C or Fortran source code can be a frustrating experience for new programmers. This “hands-on” tutorial will introduce the basic steps for compiling small- to medium-sized projects. Topics include working with multiple source files, header files, and external libraries, options for debugging and optimization, and automation using Make and Autotools (configure). For simplicity, we will only cover the build process for systems with a Linux operating system (such as the BU Shared Computing Cluster).

> Familiarity with the Linux command line is assumed. Familiarity with C or Fortran will be helpful, but is not required.
